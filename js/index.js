$(function () {
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="popover"]').popover();
		$('.carousel').carousel({
			interval: 3000
		});

		$('#contacto').on('show.bs.modal', function(e){
			$('#contactBtn').prop('disabled', true);
			$('#contactBtn').removeClass('btn-outline-success');
			$('#contactBtn').addClass('btn-primary');
		});
		$('#contacto').on('hide.bs.modal', function(e){
			$('#contactBtn').prop('disabled', false);
			$('#contactBtn').removeClass('btn-primary');
			$('#contactBtn').addClass('btn-outline-success');
		});
})